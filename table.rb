require 'erb'
require 'json'
require 'byebug'

table = nil

values = {
  distance: [-1, 0, 1, 100000],
  time: [-1, 0, 1, 100000],
  planned_distance: [-1, 0, 1, 100000],
  planned_time: [-1, 0, 1, 100000],
  type: [:budget, :luxury, :kek],
  plan: [:minute, :fixed_price, :kek],
  inno_discount: [:yes, :no, :kek],
}.each do |key, values|
  if table.nil?
    table = values.map { |v| { key => v } }
  else
    new_table = []
    table.each do |row|
      values.each do |v|
        new_row = row.dup
        new_row[key] = v
        new_table << new_row
      end
    end
    table = new_table
  end
end

def invalid?(value)
  value == :kek || value == -1 || value == 0
end

def valid?(value)
  !invalid?(value)
end

special_cases = [
  -> (row) { invalid?(row[:type]) },
  -> (row) { invalid?(row[:inno_discount]) },
  -> (row) { invalid?(row[:plan]) },
  -> (row) { row[:type] == :luxury && row[:plan] == :fixed_price },
  -> (row) { row[:plan] == :fixed_price && invalid?(row[:planned_distance]) },
  -> (row) { row[:plan] == :fixed_price && row[:planned_distance] == 0 },
  -> (row) { row[:plan] == :fixed_price && invalid?(row[:planned_time]) },
  -> (row) { row[:plan] == :fixed_price && row[:planned_time] == 0 },
  -> (row) { row[:plan] == :fixed_price && invalid?(row[:planned_distance]) && invalid?(row[:planned_time]) },
  -> (row) { row[:plan] == :fixed_price && invalid?(row[:distance]) },
  -> (row) { invalid?(row[:time]) },
  -> (row) { row[:time] == 1 && row[:plan] == :minute && [:budget, :luxury].member?(row[:type]) && [:yes, :no].member?(row[:inno_discount]) },
  -> (row) { row[:time] == 100000 && row[:plan] == :minute && [:budget, :luxury].member?(row[:type]) && [:yes, :no].member?(row[:inno_discount]) },
]

valid_examples = table.reject do |row|
  special_cases.any? { |c| c.call(row) }
end

def make_row(**values)
  {
    distance: :-,
    time: :-,
    planned_distance: :-,
    planned_time: :-,
    type: :-,
    plan: :-,
    inno_discount: :-,
  }.merge(values)
end

table =
[:kek].map { |v| make_row(type: v) } +
[:kek].map { |v| make_row(inno_discount: v) } +
[:kek].map { |v| make_row(plan: v) } +
[-1, 0].map { |v| make_row(time: v) } +
[make_row(type: :luxury, plan: :fixed_price)] +
[-1, 0].map { |v| make_row(planned_distance: v, plan: :fixed_price) } +
[-1, 0].map { |v| make_row(distance: v, plan: :fixed_price) } +
[-1, 0].map { |v| make_row(planned_time: v, plan: :fixed_price) } +
[-1, 0].map { |v| make_row(planned_distance: v, plan: :fixed_price) }
  .flat_map { |row| [1, 100000].map { |v| row.merge(planned_time: v) } } +
[make_row(type: :budget, time: 1, plan: :minute, inno_discount: :no, expected_result: {:price=>29.0})] +
[make_row(type: :budget, time: 1, plan: :minute, inno_discount: :yes, expected_result: {:price=>25.52})] +
[make_row(type: :luxury, time: 1, plan: :minute, inno_discount: :no, expected_result: {:price=>74.0})] +
[make_row(type: :luxury, time: 1, plan: :minute, inno_discount: :yes, expected_result: {:price=>65.12})] +
[make_row(type: :budget, time: 100000, plan: :minute, inno_discount: :no, expected_result: {:price=>29.0 * 100000})] +
[make_row(type: :budget, time: 100000, plan: :minute, inno_discount: :yes, expected_result: {:price=>25.52 * 100000})] +
[make_row(type: :luxury, time: 100000, plan: :minute, inno_discount: :no, expected_result: {:price=>74.0 * 100000})] +
[make_row(type: :luxury, time: 100000, plan: :minute, inno_discount: :yes, expected_result: {:price=>65.12 * 100000})]

table.each do |row|
  row[:expected_result] ||= 'Invalid Request'
end

table += valid_examples

table.each do |row|
  next if row[:expected_result]

  per_minute = case row[:type]
  when :budget
    29
  when :luxury 
    74    
  end

  max_deviation = 1.14000000000000002
  price = case row[:plan]
  when :minute    
    row[:time] * per_minute
  when :fixed_price
    if (row[:distance] / row[:planned_distance] <= max_deviation) && (row[:time] / row[:planned_time] <= max_deviation)
      10 * row[:distance]
    else
      row[:time] * per_minute
    end
  end

  inno_discount = 0.12
  if row[:inno_discount] == :yes
    price *= (1 - inno_discount)    
  end

  row[:expected_result] = price < 0 ? 'Invalid Request' : {price: price.to_f}
rescue StandardError => e
  row[:expected_result] = 'Invalid Request'
end

string = table.map { |row| row.values.join(',') }.join("\n")
File.write('table.csv', string)

params = table.map do |row|
  p = row.dup
  p.delete(:expected_result)
  p.each_key do |k|
    v = row[k]
    p[k] = :lol if v == :-
  end
  p
end

spec = ERB.new(<<~SPEC, trim_mode: "-")
  require 'rspec'
  require 'net/http'
  require 'uri'
  require 'json'

  RSpec.configure do |config|
    config.example_status_persistence_file_path = '.rspec_status'
  end

  RSpec.describe 'innodrive service' do
    subject(:body) do
      key = 'AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w'
      email = 'a.brisilin@innopolis.university'
      args = URI.encode_www_form(params.merge(service: 'calculatePrice', email:))
      uri = URI::HTTPS.build(host: 'script.google.com', path: '/macros/s/' + key + '/exec', query: args)
      response = Net::HTTP.get_response(uri)
      if location = response.header['location']
        response = Net::HTTP.get_response(URI(location))
      end
      response.body
    end

    after { sleep(1) }
    <% params.each_with_index do |p, i| %>
    describe 'R<%= i + 1 %> <%= p %>' do
      let(:params) { <%= p %> }
      <% expected = table[i][:expected_result] -%>
      <% if expected.is_a?(Hash) %>
      it { expect(JSON.parse(body, {symbolize_names: true})).to eq(<%= expected %>) }
      <% else %>
      it { expect(body).to eq('<%= expected %>') }
      <% end %>
    end
    <% end %>
  end
SPEC

File.write('spec.rb', spec.result(binding))

File.open('table.md', 'w') do |final_table|
  results = table.map { |row| row[:expected_result] }.uniq

  final_table.write("| Conditions | Values |" + table.each_with_index.map { |_,i| " R#{i + 1}"}.join(' | ') + " |\n")
  final_table.write("| - | - |" + table.map { |_| "-"}.join(' | ') + " |\n")

  values.each do |key, v|
    final_table.write("| #{key}|#{v.join(', ')} | " + table.map { |row| row[key] == :- ? '*' : row[key] }.join(' | ')+ " |\n")
  end

  results.each do |result|
    final_table.write("| #{JSON.dump(result)} | " + table.map { |row| row[:expected_result] == result ? 'X' : ' ' }.join(' | ')+ " |\n")
  end
end
