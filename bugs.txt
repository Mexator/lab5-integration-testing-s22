.................FFF.FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

Failures:

  1) innodrive service R18 {:distance=>:lol, :time=>1, :planned_distance=>:lol, :planned_time=>:lol, :type=>:budget, :plan=>:minute, :inno_discount=>:yes} is expected to eq {:price=>25.52}
     Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>25.52}) }

       expected: {:price=>25.52}
            got: {:price=>26.68}

       (compared using ==)

       Diff:
       @@ -1 +1 @@
       -:price => 25.52,
       +:price => 26.68,
     # ./spec.rb:147:in `block (3 levels) in <top (required)>'

  2) innodrive service R19 {:distance=>:lol, :time=>1, :planned_distance=>:lol, :planned_time=>:lol, :type=>:luxury, :plan=>:minute, :inno_discount=>:no} is expected to eq {:price=>74.0}
     Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>74.0}) }

       expected: {:price=>74.0}
            got: {:price=>51.8}

       (compared using ==)

       Diff:
       @@ -1 +1 @@
       -:price => 74.0,
       +:price => 51.8,
     # ./spec.rb:154:in `block (3 levels) in <top (required)>'

  3) innodrive service R20 {:distance=>:lol, :time=>1, :planned_distance=>:lol, :planned_time=>:lol, :type=>:luxury, :plan=>:minute, :inno_discount=>:yes} is expected to eq {:price=>65.12}
     Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>65.12}) }

       expected: {:price=>65.12}
            got: {:price=>47.656}

       (compared using ==)

       Diff:
       @@ -1 +1 @@
       -:price => 65.12,
       +:price => 47.656,
     # ./spec.rb:161:in `block (3 levels) in <top (required)>'

  4) innodrive service R22 {:distance=>:lol, :time=>100000, :planned_distance=>:lol, :planned_time=>:lol, :type=>:budget, :plan=>:minute, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
     Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2552000.0}) }

       expected: {:price=>2552000.0}
            got: {:price=>2668000}

       (compared using ==)

       Diff:
       @@ -1 +1 @@
       -:price => 2552000.0,
       +:price => 2668000,
     # ./spec.rb:175:in `block (3 levels) in <top (required)>'

  5) innodrive service R23 {:distance=>:lol, :time=>100000, :planned_distance=>:lol, :planned_time=>:lol, :type=>:luxury, :plan=>:minute, :inno_discount=>:no} is expected to eq {:price=>7400000.0}
     Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>7400000.0}) }

       expected: {:price=>7400000.0}
            got: {:price=>5180000}

       (compared using ==)

       Diff:
       @@ -1 +1 @@
       -:price => 7400000.0,
       +:price => 5180000,
     # ./spec.rb:182:in `block (3 levels) in <top (required)>'

  6) innodrive service R24 {:distance=>:lol, :time=>100000, :planned_distance=>:lol, :planned_time=>:lol, :type=>:luxury, :plan=>:minute, :inno_discount=>:yes} is expected to eq {:price=>6512000.0}
     Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>6512000.0}) }

       expected: {:price=>6512000.0}
            got: {:price=>4765600}

       (compared using ==)

       Diff:
       @@ -1 +1 @@
       -:price => 6512000.0,
       +:price => 4765600,
     # ./spec.rb:189:in `block (3 levels) in <top (required)>'

  7) innodrive service R25 {:distance=>1, :time=>1, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
     Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>8.8}) }

       expected: {:price=>8.8}
            got: {:price=>11.5}

       (compared using ==)

       Diff:
       @@ -1 +1 @@
       -:price => 8.8,
       +:price => 11.5,
     # ./spec.rb:196:in `block (3 levels) in <top (required)>'

  8) innodrive service R26 {:distance=>1, :time=>1, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
     Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>10.0}) }

       expected: {:price=>10.0}
            got: {:price=>12.5}

       (compared using ==)

       Diff:
       @@ -1 +1 @@
       -:price => 10.0,
       +:price => 12.5,
     # ./spec.rb:203:in `block (3 levels) in <top (required)>'

  9) innodrive service R27 {:distance=>1, :time=>1, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
     Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>8.8}) }

       expected: {:price=>8.8}
            got: {:price=>15.333333333333336}

       (compared using ==)

       Diff:
       @@ -1 +1 @@
       -:price => 8.8,
       +:price => 15.333333333333336,
     # ./spec.rb:210:in `block (3 levels) in <top (required)>'

  10) innodrive service R28 {:distance=>1, :time=>1, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>10.0}) }

        expected: {:price=>10.0}
             got: {:price=>16.666666666666668}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 10.0,
        +:price => 16.666666666666668,
      # ./spec.rb:217:in `block (3 levels) in <top (required)>'

  11) innodrive service R29 {:distance=>1, :time=>1, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>8.8}) }

        expected: {:price=>8.8}
             got: {:price=>15.333333333333336}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 8.8,
        +:price => 15.333333333333336,
      # ./spec.rb:224:in `block (3 levels) in <top (required)>'

  12) innodrive service R30 {:distance=>1, :time=>1, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>10.0}) }

        expected: {:price=>10.0}
             got: {:price=>16.666666666666668}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 10.0,
        +:price => 16.666666666666668,
      # ./spec.rb:231:in `block (3 levels) in <top (required)>'

  13) innodrive service R31 {:distance=>1, :time=>1, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>8.8}) }

        expected: {:price=>8.8}
             got: {:price=>15.333333333333336}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 8.8,
        +:price => 15.333333333333336,
      # ./spec.rb:238:in `block (3 levels) in <top (required)>'

  14) innodrive service R32 {:distance=>1, :time=>1, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>10.0}) }

        expected: {:price=>10.0}
             got: {:price=>16.666666666666668}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 10.0,
        +:price => 16.666666666666668,
      # ./spec.rb:245:in `block (3 levels) in <top (required)>'

  15) innodrive service R33 {:distance=>1, :time=>100000, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2552000.0}) }

        expected: {:price=>2552000.0}
             got: {:price=>1533333.3333333335}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2552000.0,
        +:price => 1533333.3333333335,
      # ./spec.rb:252:in `block (3 levels) in <top (required)>'

  16) innodrive service R34 {:distance=>1, :time=>100000, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2900000.0}) }

        expected: {:price=>2900000.0}
             got: {:price=>1666666.6666666667}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2900000.0,
        +:price => 1666666.6666666667,
      # ./spec.rb:259:in `block (3 levels) in <top (required)>'

  17) innodrive service R35 {:distance=>1, :time=>100000, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>8.8}) }

        expected: {:price=>8.8}
             got: {:price=>11.5}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 8.8,
        +:price => 11.5,
      # ./spec.rb:266:in `block (3 levels) in <top (required)>'

  18) innodrive service R36 {:distance=>1, :time=>100000, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>10.0}) }

        expected: {:price=>10.0}
             got: {:price=>12.5}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 10.0,
        +:price => 12.5,
      # ./spec.rb:273:in `block (3 levels) in <top (required)>'

  19) innodrive service R37 {:distance=>1, :time=>100000, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2552000.0}) }

        expected: {:price=>2552000.0}
             got: {:price=>1533333.3333333335}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2552000.0,
        +:price => 1533333.3333333335,
      # ./spec.rb:280:in `block (3 levels) in <top (required)>'

  20) innodrive service R38 {:distance=>1, :time=>100000, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2900000.0}) }

        expected: {:price=>2900000.0}
             got: {:price=>1666666.6666666667}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2900000.0,
        +:price => 1666666.6666666667,
      # ./spec.rb:287:in `block (3 levels) in <top (required)>'

  21) innodrive service R39 {:distance=>1, :time=>100000, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>8.8}) }

        expected: {:price=>8.8}
             got: {:price=>1533333.3333333335}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 8.8,
        +:price => 1533333.3333333335,
      # ./spec.rb:294:in `block (3 levels) in <top (required)>'

  22) innodrive service R40 {:distance=>1, :time=>100000, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>10.0}) }

        expected: {:price=>10.0}
             got: {:price=>1666666.6666666667}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 10.0,
        +:price => 1666666.6666666667,
      # ./spec.rb:301:in `block (3 levels) in <top (required)>'

  23) innodrive service R41 {:distance=>100000, :time=>1, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>25.52}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>25.52}) }

        expected: {:price=>25.52}
             got: {:price=>15.333333333333336}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 25.52,
        +:price => 15.333333333333336,
      # ./spec.rb:308:in `block (3 levels) in <top (required)>'

  24) innodrive service R42 {:distance=>100000, :time=>1, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>29.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>29.0}) }

        expected: {:price=>29.0}
             got: {:price=>16.666666666666668}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 29.0,
        +:price => 16.666666666666668,
      # ./spec.rb:315:in `block (3 levels) in <top (required)>'

  25) innodrive service R43 {:distance=>100000, :time=>1, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>25.52}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>25.52}) }

        expected: {:price=>25.52}
             got: {:price=>15.333333333333336}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 25.52,
        +:price => 15.333333333333336,
      # ./spec.rb:322:in `block (3 levels) in <top (required)>'

  26) innodrive service R44 {:distance=>100000, :time=>1, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>29.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>29.0}) }

        expected: {:price=>29.0}
             got: {:price=>16.666666666666668}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 29.0,
        +:price => 16.666666666666668,
      # ./spec.rb:329:in `block (3 levels) in <top (required)>'

  27) innodrive service R45 {:distance=>100000, :time=>1, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>880000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>880000.0}) }

        expected: {:price=>880000.0}
             got: {:price=>1150000}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 880000.0,
        +:price => 1150000,
      # ./spec.rb:336:in `block (3 levels) in <top (required)>'

  28) innodrive service R46 {:distance=>100000, :time=>1, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>1000000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>1000000.0}) }

        expected: {:price=>1000000.0}
             got: {:price=>1250000}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 1000000.0,
        +:price => 1250000,
      # ./spec.rb:343:in `block (3 levels) in <top (required)>'

  29) innodrive service R47 {:distance=>100000, :time=>1, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>880000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>880000.0}) }

        expected: {:price=>880000.0}
             got: {:price=>15.333333333333336}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 880000.0,
        +:price => 15.333333333333336,
      # ./spec.rb:350:in `block (3 levels) in <top (required)>'

  30) innodrive service R48 {:distance=>100000, :time=>1, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>1000000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>1000000.0}) }

        expected: {:price=>1000000.0}
             got: {:price=>16.666666666666668}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 1000000.0,
        +:price => 16.666666666666668,
      # ./spec.rb:357:in `block (3 levels) in <top (required)>'

  31) innodrive service R49 {:distance=>100000, :time=>100000, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2552000.0}) }

        expected: {:price=>2552000.0}
             got: {:price=>1533333.3333333335}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2552000.0,
        +:price => 1533333.3333333335,
      # ./spec.rb:364:in `block (3 levels) in <top (required)>'

  32) innodrive service R50 {:distance=>100000, :time=>100000, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2900000.0}) }

        expected: {:price=>2900000.0}
             got: {:price=>1666666.6666666667}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2900000.0,
        +:price => 1666666.6666666667,
      # ./spec.rb:371:in `block (3 levels) in <top (required)>'

  33) innodrive service R51 {:distance=>100000, :time=>100000, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2552000.0}) }

        expected: {:price=>2552000.0}
             got: {:price=>1533333.3333333335}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2552000.0,
        +:price => 1533333.3333333335,
      # ./spec.rb:378:in `block (3 levels) in <top (required)>'

  34) innodrive service R52 {:distance=>100000, :time=>100000, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2900000.0}) }

        expected: {:price=>2900000.0}
             got: {:price=>1666666.6666666667}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2900000.0,
        +:price => 1666666.6666666667,
      # ./spec.rb:385:in `block (3 levels) in <top (required)>'

  35) innodrive service R53 {:distance=>100000, :time=>100000, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2552000.0}) }

        expected: {:price=>2552000.0}
             got: {:price=>1533333.3333333335}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2552000.0,
        +:price => 1533333.3333333335,
      # ./spec.rb:392:in `block (3 levels) in <top (required)>'

  36) innodrive service R54 {:distance=>100000, :time=>100000, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>2900000.0}) }

        expected: {:price=>2900000.0}
             got: {:price=>1666666.6666666667}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 2900000.0,
        +:price => 1666666.6666666667,
      # ./spec.rb:399:in `block (3 levels) in <top (required)>'

  37) innodrive service R55 {:distance=>100000, :time=>100000, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>880000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>880000.0}) }

        expected: {:price=>880000.0}
             got: {:price=>1150000}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 880000.0,
        +:price => 1150000,
      # ./spec.rb:406:in `block (3 levels) in <top (required)>'

  38) innodrive service R56 {:distance=>100000, :time=>100000, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>1000000.0}
      Failure/Error: it { expect(JSON.parse(body, {symbolize_names: true})).to eq({:price=>1000000.0}) }

        expected: {:price=>1000000.0}
             got: {:price=>1250000}

        (compared using ==)

        Diff:
        @@ -1 +1 @@
        -:price => 1000000.0,
        +:price => 1250000,
      # ./spec.rb:413:in `block (3 levels) in <top (required)>'

Finished in 2 minutes 11.4 seconds (files took 0.09945 seconds to load)
56 examples, 38 failures

Failed examples:

rspec ./spec.rb:147 # innodrive service R18 {:distance=>:lol, :time=>1, :planned_distance=>:lol, :planned_time=>:lol, :type=>:budget, :plan=>:minute, :inno_discount=>:yes} is expected to eq {:price=>25.52}
rspec ./spec.rb:154 # innodrive service R19 {:distance=>:lol, :time=>1, :planned_distance=>:lol, :planned_time=>:lol, :type=>:luxury, :plan=>:minute, :inno_discount=>:no} is expected to eq {:price=>74.0}
rspec ./spec.rb:161 # innodrive service R20 {:distance=>:lol, :time=>1, :planned_distance=>:lol, :planned_time=>:lol, :type=>:luxury, :plan=>:minute, :inno_discount=>:yes} is expected to eq {:price=>65.12}
rspec ./spec.rb:175 # innodrive service R22 {:distance=>:lol, :time=>100000, :planned_distance=>:lol, :planned_time=>:lol, :type=>:budget, :plan=>:minute, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
rspec ./spec.rb:182 # innodrive service R23 {:distance=>:lol, :time=>100000, :planned_distance=>:lol, :planned_time=>:lol, :type=>:luxury, :plan=>:minute, :inno_discount=>:no} is expected to eq {:price=>7400000.0}
rspec ./spec.rb:189 # innodrive service R24 {:distance=>:lol, :time=>100000, :planned_distance=>:lol, :planned_time=>:lol, :type=>:luxury, :plan=>:minute, :inno_discount=>:yes} is expected to eq {:price=>6512000.0}
rspec ./spec.rb:196 # innodrive service R25 {:distance=>1, :time=>1, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
rspec ./spec.rb:203 # innodrive service R26 {:distance=>1, :time=>1, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
rspec ./spec.rb:210 # innodrive service R27 {:distance=>1, :time=>1, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
rspec ./spec.rb:217 # innodrive service R28 {:distance=>1, :time=>1, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
rspec ./spec.rb:224 # innodrive service R29 {:distance=>1, :time=>1, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
rspec ./spec.rb:231 # innodrive service R30 {:distance=>1, :time=>1, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
rspec ./spec.rb:238 # innodrive service R31 {:distance=>1, :time=>1, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
rspec ./spec.rb:245 # innodrive service R32 {:distance=>1, :time=>1, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
rspec ./spec.rb:252 # innodrive service R33 {:distance=>1, :time=>100000, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
rspec ./spec.rb:259 # innodrive service R34 {:distance=>1, :time=>100000, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
rspec ./spec.rb:266 # innodrive service R35 {:distance=>1, :time=>100000, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
rspec ./spec.rb:273 # innodrive service R36 {:distance=>1, :time=>100000, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
rspec ./spec.rb:280 # innodrive service R37 {:distance=>1, :time=>100000, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
rspec ./spec.rb:287 # innodrive service R38 {:distance=>1, :time=>100000, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
rspec ./spec.rb:294 # innodrive service R39 {:distance=>1, :time=>100000, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>8.8}
rspec ./spec.rb:301 # innodrive service R40 {:distance=>1, :time=>100000, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>10.0}
rspec ./spec.rb:308 # innodrive service R41 {:distance=>100000, :time=>1, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>25.52}
rspec ./spec.rb:315 # innodrive service R42 {:distance=>100000, :time=>1, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>29.0}
rspec ./spec.rb:322 # innodrive service R43 {:distance=>100000, :time=>1, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>25.52}
rspec ./spec.rb:329 # innodrive service R44 {:distance=>100000, :time=>1, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>29.0}
rspec ./spec.rb:336 # innodrive service R45 {:distance=>100000, :time=>1, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>880000.0}
rspec ./spec.rb:343 # innodrive service R46 {:distance=>100000, :time=>1, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>1000000.0}
rspec ./spec.rb:350 # innodrive service R47 {:distance=>100000, :time=>1, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>880000.0}
rspec ./spec.rb:357 # innodrive service R48 {:distance=>100000, :time=>1, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>1000000.0}
rspec ./spec.rb:364 # innodrive service R49 {:distance=>100000, :time=>100000, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
rspec ./spec.rb:371 # innodrive service R50 {:distance=>100000, :time=>100000, :planned_distance=>1, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
rspec ./spec.rb:378 # innodrive service R51 {:distance=>100000, :time=>100000, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
rspec ./spec.rb:385 # innodrive service R52 {:distance=>100000, :time=>100000, :planned_distance=>1, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
rspec ./spec.rb:392 # innodrive service R53 {:distance=>100000, :time=>100000, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>2552000.0}
rspec ./spec.rb:399 # innodrive service R54 {:distance=>100000, :time=>100000, :planned_distance=>100000, :planned_time=>1, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>2900000.0}
rspec ./spec.rb:406 # innodrive service R55 {:distance=>100000, :time=>100000, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:yes} is expected to eq {:price=>880000.0}
rspec ./spec.rb:413 # innodrive service R56 {:distance=>100000, :time=>100000, :planned_distance=>100000, :planned_time=>100000, :type=>:budget, :plan=>:fixed_price, :inno_discount=>:no} is expected to eq {:price=>1000000.0}

